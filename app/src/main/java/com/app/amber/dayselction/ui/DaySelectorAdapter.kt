package com.app.amber.dayselction.ui

import android.app.TimePickerDialog
import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TimePicker
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.app.amber.dayselction.R
import com.app.amber.dayselction.model.WorkingDayModel
import com.app.amber.dayselction.onDayViewClickListener
import com.app.amber.dayselction.util.convertTime
import com.app.amber.dayselction.util.toast
import com.google.android.material.chip.Chip
import java.util.*

class DaySelectorAdapter(private val dayModels: MutableList<WorkingDayModel>) :
    RecyclerView.Adapter<DaySelectorAdapter.ViewHolder>() {

    private var context: Context? = null
    private var dayViewClickListener: onDayViewClickListener? = null
    fun setOnClickListener(clickListener: onDayViewClickListener?) {
        dayViewClickListener = clickListener
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val itemLayoutView = LayoutInflater.from(parent.context).inflate(
            R.layout.week_adapter_item, parent, false
        )
        context = parent.context
        return ViewHolder(itemLayoutView)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val stItem = dayModels[position]
        if (position == 0) {
            viewHolder.remove_btn.visibility = View.GONE
        } else {
            viewHolder.remove_btn.visibility = View.VISIBLE
        }
        if (stItem.day == "0") {
            viewHolder.add_btn.visibility = View.GONE
        } else if (position == itemCount - 1) {
            viewHolder.add_btn.visibility = View.VISIBLE
        } else viewHolder.add_btn.visibility = View.GONE
        viewHolder.day_0.isChecked = stItem.day!!.contains("0")
        viewHolder.day_1.isChecked = stItem.day!!.contains("1")
        viewHolder.day_2.isChecked = stItem.day!!.contains("2")
        viewHolder.day_3.isChecked = stItem.day!!.contains("3")
        viewHolder.day_4.isChecked = stItem.day!!.contains("4")
        viewHolder.day_5.isChecked = stItem.day!!.contains("5")
        viewHolder.day_6.isChecked = stItem.day!!.contains("6")
        viewHolder.day_7.isChecked = stItem.day!!.contains("7")
        viewHolder.day_0.isEnabled = stItem.allDaySelected
        viewHolder.day_1.isEnabled = stItem.sundaySelected
        viewHolder.day_2.isEnabled = stItem.mondaySelected
        viewHolder.day_3.isEnabled = stItem.tuesdaySelected
        viewHolder.day_4.isEnabled = stItem.wednesSelected
        viewHolder.day_5.isEnabled = stItem.thursdaySelected
        viewHolder.day_6.isEnabled = stItem.fridaySelected
        viewHolder.day_7.isEnabled = stItem.saturdaySelected
        viewHolder.day_0.setOnClickListener(onDayClickListener(stItem, position))
        viewHolder.day_1.setOnClickListener(onDayClickListener(stItem, position))
        viewHolder.day_2.setOnClickListener(onDayClickListener(stItem, position))
        viewHolder.day_3.setOnClickListener(onDayClickListener(stItem, position))
        viewHolder.day_4.setOnClickListener(onDayClickListener(stItem, position))
        viewHolder.day_5.setOnClickListener(onDayClickListener(stItem, position))
        viewHolder.day_6.setOnClickListener(onDayClickListener(stItem, position))
        viewHolder.day_7.setOnClickListener(onDayClickListener(stItem, position))
        viewHolder.start_time.text = "From : " + convertTime(stItem.start_hour!!)
        viewHolder.end_time.text = "To : " + convertTime(stItem.end_hour!!)
        viewHolder.start_time_layout.setOnClickListener {
            if (!stItem.day!!.isEmpty()) showTimePickerDialog(position, false) else {
                context!!.toast("Select day")
            }
        }
        viewHolder.end_time_layout.setOnClickListener {
            if (TextUtils.isEmpty(stItem.start_hour)) {
                context!!.toast("Fill start time!!")
            } else {
                showTimePickerDialog(position, true)
            }
        }
        viewHolder.remove_btn.setOnClickListener { dayViewClickListener!!.onRemoveItem(stItem) }
        viewHolder.add_btn.setOnClickListener {
            if (!stItem.start_hour!!.isEmpty() && !stItem.end_hour!!.isEmpty()) dayViewClickListener!!.onAddItem() else {
                context!!.toast("Fill start time & end Time!!")
            }
        }
    }

    private fun showTimePickerDialog(pos: Int, isEndTime: Boolean) {
        val mHour: Int
        val mMinute: Int
        val c = Calendar.getInstance()
        mHour = c[Calendar.HOUR_OF_DAY]
        mMinute = c[Calendar.MINUTE]
        val timePickerDialog = TimePickerDialog(context,
            { view: TimePicker?, hourOfDay: Int, minute: Int ->
                val dayModel = dayModels[pos]
                val startTime = dayModel.start_hour
                if (isEndTime) {
                    if (startTime !== "") {
                        val parts = startTime!!.split(":").toTypedArray()
                        val startTimeInstance = Calendar.getInstance()
                        startTimeInstance.add(Calendar.HOUR_OF_DAY, parts[0].toInt())
                        startTimeInstance.add(Calendar.MINUTE, parts[1].toInt())
                        val endTimeInstance = Calendar.getInstance()
                        endTimeInstance.add(Calendar.HOUR_OF_DAY, hourOfDay)
                        endTimeInstance.add(Calendar.MINUTE, minute)
                        if (endTimeInstance.after(startTimeInstance)) {
                            val difference_In_Hours = (endTimeInstance.timeInMillis
                                    - startTimeInstance.timeInMillis) / (1000 * 60 * 60) % 24
                            if (difference_In_Hours >= 1) {
                                dayModel.end_hour = "$hourOfDay:$minute"
                            } else {
                                context!!.toast(
                                    "End Time should be more than 1 hour!!")
                            }
                        } else {
                            context!!.toast("End Time should in same day!!")
                        }
                    }
                } else dayModel.start_hour = "$hourOfDay:$minute"
                dayModels[pos] = dayModel
                notifyDataSetChanged()
            }, mHour, mMinute, false
        )
        timePickerDialog.show()
    }



    override fun getItemCount(): Int {
        return dayModels.size
    }

    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var day_0: Chip
        var day_1: Chip
        var day_2: Chip
        var day_3: Chip
        var day_4: Chip
        var day_5: Chip
        var day_6: Chip
        var day_7: Chip
        var add_btn: AppCompatImageView
        var remove_btn: AppCompatImageView
        var start_time: AppCompatTextView
        var end_time: AppCompatTextView
        var end_time_layout: LinearLayout
        var start_time_layout: LinearLayout

        init {
            add_btn = itemLayoutView.findViewById(R.id.add_btn)
            remove_btn = itemLayoutView.findViewById(R.id.remove_btn)
            day_0 = itemLayoutView.findViewById(R.id.day_0)
            day_1 = itemLayoutView.findViewById(R.id.day_1)
            day_2 = itemLayoutView.findViewById(R.id.day_2)
            day_3 = itemLayoutView.findViewById(R.id.day_3)
            day_4 = itemLayoutView.findViewById(R.id.day_4)
            day_5 = itemLayoutView.findViewById(R.id.day_5)
            day_6 = itemLayoutView.findViewById(R.id.day_6)
            day_7 = itemLayoutView.findViewById(R.id.day_7)
            end_time_layout = itemLayoutView.findViewById(R.id.end_time_layout)
            start_time_layout = itemLayoutView.findViewById(R.id.start_time_layout)
            start_time = itemLayoutView.findViewById(R.id.start_time)
            end_time = itemLayoutView.findViewById(R.id.end_time)
        }
    }

    internal inner class onDayClickListener(var dayModel: WorkingDayModel, var position: Int) :
        View.OnClickListener {
        override fun onClick(v: View) {
            dayViewClickListener!!.checkDay(
                dayModel,
                position,
                v.tag.toString(),
                (v as Chip).isChecked
            )
            notifyDataSetChanged()
        }
    }
}