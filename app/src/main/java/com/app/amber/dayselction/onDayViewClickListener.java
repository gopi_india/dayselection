package com.app.amber.dayselction;

import com.app.amber.dayselction.model.WorkingDayModel;

public interface onDayViewClickListener {

    void onAddItem();

    void onRemoveItem(WorkingDayModel day);

    void checkDay(WorkingDayModel day, int index, String checkedValue, boolean isChecked);
}