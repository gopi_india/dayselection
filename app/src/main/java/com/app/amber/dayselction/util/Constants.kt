package com.app.amber.dayselction.util

import android.text.TextUtils
import com.app.amber.dayselction.model.WorkingDayModel
import java.text.SimpleDateFormat
import java.util.*

object Constants {
    const val configIdIntent = "configId"

    const val LOCATION_REQUEST = 100
    const val GPS_REQUEST = 101


     lateinit var savedDayModel : List<WorkingDayModel>

}