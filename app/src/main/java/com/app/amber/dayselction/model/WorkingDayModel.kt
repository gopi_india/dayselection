package com.app.amber.dayselction.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import kotlinx.android.parcel.Parcelize

@Parcelize
class WorkingDayModel : Parcelable {
    var day: String? = ""
    var start_hour: String? = ""
    var end_hour: String? = ""
    var allDaySelected: Boolean = true
    var sundaySelected: Boolean = true
    var mondaySelected: Boolean = true
    var tuesdaySelected: Boolean = true
    var wednesSelected: Boolean = true
    var thursdaySelected: Boolean = true
    var fridaySelected: Boolean = true
    var saturdaySelected: Boolean = true
}