package com.mayowa.android.locationwithlivedata

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.amber.dayselction.databinding.DayselectorFragmentBinding
import com.app.amber.dayselction.model.DayModel
import com.app.amber.dayselction.model.Days
import com.app.amber.dayselction.model.WorkingDayModel
import com.app.amber.dayselction.onDayViewClickListener
import com.app.amber.dayselction.onEventListener
import com.app.amber.dayselction.ui.DaySelectorAdapter
import com.app.amber.dayselction.util.Constants.configIdIntent
import com.app.amber.dayselction.util.RoundedBottomSheetDialogFragment
import com.app.amber.dayselction.util.joinExistingValue
import com.app.amber.dayselction.util.removeIndexedValue
import com.google.gson.Gson
import com.google.gson.GsonBuilder


class DaySelectorFragment : RoundedBottomSheetDialogFragment(), onDayViewClickListener {


    private var configId: String? = null
    lateinit var _binding: DayselectorFragmentBinding
    var modelList: MutableList<WorkingDayModel> = ArrayList()
    var listener: onEventListener? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            configId = it.getString(configIdIntent)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DayselectorFragmentBinding.inflate(inflater, container, false)
        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val model = WorkingDayModel()
        modelList.add(model)
        val adapter = DaySelectorAdapter(modelList)

        adapter.setOnClickListener(this)
        _binding.recyclerView.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        _binding.recyclerView.setAdapter(adapter)

        _binding.submitBtn.setOnClickListener {

            if (modelList.any {
                    it.day!!.isEmpty()
                            || it.start_hour!!.isEmpty()
                            || it.end_hour!!.isEmpty()
                }) {
                Toast.makeText(requireContext(), "Fill all Days", Toast.LENGTH_SHORT).show()
            } else {
                val gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
                var gsonList: ArrayList<DayModel> = ArrayList<DayModel>()
                modelList.forEach {
                    var model = DayModel()
                    var days = Days()
                    model.days = listOf(it.day!!)
                    days.start_hours = it.start_hour!!
                    days.end_hours = it.end_hour!!
                    model.hours = listOf(days)
                    gsonList.add(model)
                }
                listener?.onDataReceived(gson.toJson(gsonList))
                dismiss()
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = activity as onEventListener
        } catch (e: ClassCastException) {
            throw ClassCastException("$activity must implement onSomeEventListener")
        }
    }


    companion object {
        @JvmStatic
        fun newInstance() =
            DaySelectorFragment().apply {
            }

        const val TAG = "DAYSELECTOR"
    }

    override fun onAddItem() {
        val dayModel = WorkingDayModel()
        for (i in modelList.indices) {
            val selectedVar = TextUtils.split(modelList[i].day, ",")
            for (item in selectedVar) {
                checkableListener(dayModel, item, false)
            }
        }
        modelList.add(dayModel)
        _binding.recyclerView.adapter!!.notifyDataSetChanged()

    }

    override fun onRemoveItem(day: WorkingDayModel?) {
        for (i in modelList.indices) {
            val selectedVar = TextUtils.split(day?.day, ",")
            for (s in selectedVar) {
                val dayModels = modelList[i]
                checkableListener(dayModels, s, true)
                modelList[i] = dayModels
            }
        }
        modelList.remove(day)
        _binding.recyclerView.adapter!!.notifyDataSetChanged()
    }

    override fun checkDay(
        day: WorkingDayModel,
        index: Int,
        checkedValue: String,
        isChecked: Boolean
    ) {
        for (i in modelList.indices) {
            if (checkedValue == "0") {
                if (isChecked) {
                    day.allDaySelected = isChecked
                    day.day = "0"
                } else day.day = ""
                day.sundaySelected = !isChecked
                day.mondaySelected = !isChecked
                day.tuesdaySelected = !isChecked
                day.wednesSelected = !isChecked
                day.thursdaySelected = !isChecked
                day.fridaySelected = !isChecked
                day.saturdaySelected = !isChecked
                modelList[index] = day
            } else if (isChecked) {
                if (index == i) {
                    val selectedVar = TextUtils.split(day.day, ",")
                    checkableListener(day, checkedValue, true)
                    day.day = joinExistingValue(selectedVar, checkedValue)
                    modelList[i] = day
                } else {
                    val selectedVar = TextUtils.split(modelList[i].day, ",")
                    val dayModel = modelList[i]
                    checkableListener(dayModel, checkedValue, false)
                    dayModel.day = removeIndexedValue(selectedVar, checkedValue)
                    modelList[i] = dayModel
                }
            } else {
                val selectedVar = TextUtils.split(modelList[i].day, ",")
                val dayModel = modelList[i]
                checkableListener(dayModel, checkedValue, true)
                dayModel.day = removeIndexedValue(selectedVar, checkedValue)
                modelList[i] = dayModel
            }
        }
        Log.d("checkDay ", Gson().toJson(modelList))
    }

    private fun checkableListener(dayModel: WorkingDayModel, date: String, isSet: Boolean) {
        when (date) {
            "0" -> dayModel.allDaySelected = isSet
            "1" -> dayModel.sundaySelected = isSet
            "2" -> dayModel.mondaySelected = isSet
            "3" -> dayModel.tuesdaySelected = isSet
            "4" -> dayModel.wednesSelected = isSet
            "5" -> dayModel.thursdaySelected = isSet
            "6" -> dayModel.fridaySelected = isSet
            "7" -> dayModel.saturdaySelected = isSet
        }
    }





}