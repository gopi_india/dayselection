package com.app.amber.dayselction.model

import com.google.gson.annotations.Expose

class Days {
    @Expose
    var end_hours: String = ""
    @Expose var start_hours: String = ""
}