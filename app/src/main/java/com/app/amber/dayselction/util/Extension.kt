package com.app.amber.dayselction.util

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import java.text.SimpleDateFormat
import java.util.*


fun convertTime(_24HourTime: String): String {
    return try {
        if (TextUtils.isEmpty(_24HourTime)) {
            return ""
        }
        val _24HourSDF = SimpleDateFormat("HH:mm", Locale.getDefault())
        val _12HourSDF = SimpleDateFormat("hh:mm a", Locale.getDefault())
        val _24HourDt: Date = _24HourSDF.parse(_24HourTime)
        _12HourSDF.format(_24HourDt)
    } catch (e: Exception) {
        ""
    }
}

 fun joinExistingValue(existVal: Array<String>, value: String): String {

    val list: MutableSet<String> = HashSet(Arrays.asList(*existVal))
    list.add(value)
    val existValList = list.toTypedArray()
    return TextUtils.join(",", existValList)
}
 fun removeIndexedValue(str_array: Array<String>, value: String): String {
    val list: MutableList<String> = ArrayList(Arrays.asList(*str_array))
    list.remove(value)
    val strList = list.toTypedArray()
    return TextUtils.join(",", strList)
}

fun Context.toast(msg: String) {
    Toast.makeText(this, msg, LENGTH_SHORT).show()
}