package com.app.amber.dayselction.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import com.app.amber.dayselction.R
import com.app.amber.dayselction.onEventListener
import com.app.amber.dayselction.util.Constants
import com.app.amber.dayselction.util.toast
import com.google.gson.Gson
import com.mayowa.android.locationwithlivedata.DaySelectorFragment

class MainActivity : AppCompatActivity(), onEventListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<AppCompatButton>(R.id.days_choose_btn).setOnClickListener {
            val daySelectorFragment = DaySelectorFragment.newInstance()
            if (!daySelectorFragment.isAdded) {
                daySelectorFragment.showNow(
                    supportFragmentManager, DaySelectorFragment.TAG
                )
            }
        }


    }

    override fun onDataReceived(s: String?) {
        toast(s.toString())
        s.let {
            findViewById<AppCompatTextView>(R.id.response).setText(it)
        }
    }


}